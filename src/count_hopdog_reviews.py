import json

# Path to your JSONL file (relative to the script's location)
filename = "src/data/ratebeer-curated-subset-0.jsonl"

# Initialize a counter
count = 0

# Open and read the file
with open(filename, 'r') as file:
    for line in file:
        review = json.loads(line)
        if 'review/profileName' in review and review['review/profileName'] == 'hopdog':
            count += 1

print(f"Number of reviews by 'hopdog': {count}")
