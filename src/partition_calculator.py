import os
import time
import dask.bag as db
import dask.dataframe as dd
import json
import pandas as pd

# Path to your JSONL file
file_path = 'src/data/ratebeer-proper-line-seperated.jsonl'

# Get the size of the file in bytes
file_size_bytes = os.path.getsize(file_path)

# Convert bytes to megabytes
file_size_mb = file_size_bytes / (1024 * 1024)

# List of different partition sizes to consider (in MB)
partition_sizes_mb = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500]

print(f"Total file size: {file_size_mb:.2f} MB")

# Define the data types explicitly
dtypes = {
    'beer/name': 'object',
    'beer/beerId': 'int64',
    'beer/brewerId': 'int64',
    'beer/ABV': 'float64',
    'beer/style': 'object',
    'review/appearance': 'float64',
    'review/aroma': 'float64',
    'review/palate': 'float64',
    'review/taste': 'float64',
    'review/overall': 'float64',
    'review/time': 'int64',
    'review/profileName': 'object',
    'review/text': 'object'
}

# Meta information for the dataframe
meta = pd.DataFrame({k: pd.Series(dtype=v) for k, v in dtypes.items()})

# Function to load JSONL into a Dask Bag
def load_jsonl_to_dask_bag(jsonl_path):
    b = db.read_text(jsonl_path).map(json.loads)
    return b

# Function to preprocess and enforce data types
def preprocess_and_enforce_dtypes(df):
    for col in df.columns:
        if col in ['beer/ABV', 'review/appearance', 'review/aroma', 'review/palate', 'review/taste', 'review/overall']:
            df[col] = pd.to_numeric(df[col], errors='coerce')  # Convert to numeric, set errors to NaN
        elif col in ['beer/beerId', 'beer/brewerId', 'review/time']:
            df[col] = pd.to_numeric(df[col], errors='coerce').astype('Int64')  # Convert to integers, set errors to NaN
        else:
            df[col] = df[col].astype(dtypes[col])
    return df

# Function to benchmark partition size
def benchmark_partition_size(partition_size_mb):
    start_time = time.time()
    try:
        # Load the data into a Dask Bag
        dask_bag = load_jsonl_to_dask_bag(file_path)
        
        # Convert the Dask Bag to a DataFrame
        df = dask_bag.to_dataframe(meta=meta)
        
        # Enforce the data types with preprocessing
        df = df.map_partitions(preprocess_and_enforce_dtypes, meta=meta)
        
        # Repartition the DataFrame based on the partition size
        df = df.repartition(partition_size=partition_size_mb * 1024 * 1024)  # Convert MB to bytes
        
        # Trigger computation to monitor performance
        df.head()
        
        # Record the time taken for loading and computing
        elapsed_time = time.time() - start_time
        print(f"Partition size: {partition_size_mb} MB -> Number of partitions: {df.npartitions}, Time taken: {elapsed_time:.2f} seconds")
    except Exception as e:
        print(f"Partition size: {partition_size_mb} MB -> Failed with error: {e}")

# Benchmark each partition size
for partition_size_mb in partition_sizes_mb:
    benchmark_partition_size(partition_size_mb)
